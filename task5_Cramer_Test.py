import numpy as np
import time
import random
import matplotlib.pyplot as plt


def cramer(equation_number):
    a = np.array([[random.randint(1, 100) for n in range(equation_number)] for k in range(equation_number)])
    b = np.array([random.randint(1, 100) for n in range(equation_number)])
    x = np.linalg.solve(a, b)
    try:
        x = np.linalg.solve(a, b)
        return x
    except np.linalg.LinAlgError:
        print('Корней нет')


times = []
sizes = []

for i in range(1, 50):
    sizes.append(i)
    time_start = time.perf_counter()
    cramer(i)
    time_end = time.perf_counter()
    times.append(time_end - time_start)

plt.plot(sizes, times)
plt.xlabel('Количество уравнений в системе')
plt.ylabel('Время вычисления, с')
plt.legend()
plt.show()
